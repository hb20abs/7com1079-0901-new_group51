Group: new_group51

Question
========

RQ: Is there a correlation between the G3 (final grade) and the walc (weekend alcohol consumption)?

Null hypothesis: There is no correlation between the G3 (final grade) and the walc (weekend alcohol consumption)

Alternative hypothesis: There is a correlation between the G3(final grade) and the walc (weekend alcohol consumption)

Dataset
=======

URL: https://www.kaggle.com/uciml/student-alcohol-consumption

Column Headings:

```

[1] "school"     "sex"        "age"        "address"    "famsize"    "Pstatus"    "Medu"       "Fedu"      
 [9] "Mjob"       "Fjob"       "reason"     "guardian"   "traveltime" "studytime"  "failures"   "schoolsup" 
[17] "famsup"     "paid"       "activities" "nursery"    "higher"     "internet"   "romantic"   "famrel"    
[25] "freetime"   "goout"      "Dalc"       "Walc"       "health"     "absences"   "G1"         "G2"        
[33] "G3"   

```